package de.hanno.hpengine.engine.model.animation

import org.joml.Matrix4f

class Bone(val boneId: Int, val boneName: String, val offsetMatrix: Matrix4f)
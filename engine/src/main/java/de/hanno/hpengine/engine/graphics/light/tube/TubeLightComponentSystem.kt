package de.hanno.hpengine.engine.graphics.light.tube

import de.hanno.hpengine.engine.manager.SimpleComponentSystem

class TubeLightComponentSystem: SimpleComponentSystem<TubeLight>(componentClass = TubeLight::class.java)
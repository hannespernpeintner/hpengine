package de.hanno.hpengine.engine.graphics.light.area

import de.hanno.hpengine.engine.backend.EngineContext
import de.hanno.hpengine.engine.backend.OpenGl
import de.hanno.hpengine.engine.backend.gpuContext
import de.hanno.hpengine.engine.backend.programManager
import de.hanno.hpengine.engine.camera.Camera
import de.hanno.hpengine.engine.entity.Entity
import de.hanno.hpengine.engine.entity.SimpleEntitySystem
import de.hanno.hpengine.engine.graphics.GpuContext
import de.hanno.hpengine.engine.graphics.buffer.PersistentMappedBuffer
import de.hanno.hpengine.engine.graphics.profiled
import de.hanno.hpengine.engine.graphics.renderer.constants.GlCap
import de.hanno.hpengine.engine.graphics.renderer.constants.GlTextureTarget
import de.hanno.hpengine.engine.graphics.renderer.constants.MagFilter
import de.hanno.hpengine.engine.graphics.renderer.constants.MinFilter
import de.hanno.hpengine.engine.graphics.renderer.constants.TextureFilterConfig
import de.hanno.hpengine.engine.graphics.renderer.drawstrategy.DrawResult
import de.hanno.hpengine.engine.graphics.renderer.drawstrategy.draw
import de.hanno.hpengine.engine.graphics.renderer.rendertarget.ColorAttachmentDefinition
import de.hanno.hpengine.engine.graphics.renderer.rendertarget.CubeMapRenderTarget
import de.hanno.hpengine.engine.graphics.renderer.rendertarget.DepthBuffer
import de.hanno.hpengine.engine.graphics.renderer.rendertarget.FrameBuffer
import de.hanno.hpengine.engine.graphics.renderer.rendertarget.RenderTarget
import de.hanno.hpengine.engine.graphics.renderer.rendertarget.toCubeMaps
import de.hanno.hpengine.engine.graphics.shader.Program
import de.hanno.hpengine.engine.graphics.state.RenderState
import de.hanno.hpengine.engine.graphics.state.RenderSystem
import de.hanno.hpengine.engine.instancing.instanceCount
import de.hanno.hpengine.engine.manager.SimpleComponentSystem
import de.hanno.hpengine.engine.model.texture.CubeMap
import de.hanno.hpengine.engine.model.texture.TextureDimension
import de.hanno.hpengine.engine.scene.Scene
import de.hanno.hpengine.util.Util
import de.hanno.struct.StructArray
import de.hanno.struct.enlarge
import kotlinx.coroutines.CoroutineScope
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL12
import org.lwjgl.opengl.GL14
import org.lwjgl.opengl.GL30
import java.nio.FloatBuffer
import java.util.ArrayList

class AreaLightComponentSystem: SimpleComponentSystem<AreaLight>(componentClass = AreaLight::class.java)

class AreaLightSystem(val engine: EngineContext) : SimpleEntitySystem(listOf(AreaLight::class.java)), RenderSystem {
    private val cameraEntity: Entity = Entity("AreaLightComponentSystem")
    private val camera = Camera(cameraEntity, Util.createPerspective(90f, 1f, 1f, 500f), 1f, 500f, 90f, 1f)
    private var gpuAreaLightArray = StructArray(size = 20) { AreaLightStruct() }

    val lightBuffer: PersistentMappedBuffer = engine.gpuContext.window.invoke { PersistentMappedBuffer(engine.gpuContext, 1000) }

    private val mapRenderTarget = CubeMapRenderTarget(engine.gpuContext, RenderTarget(
        engine.gpuContext,
        FrameBuffer(
            engine.gpuContext,
            DepthBuffer(CubeMap(
                engine.gpuContext,
                TextureDimension(AREALIGHT_SHADOWMAP_RESOLUTION, AREALIGHT_SHADOWMAP_RESOLUTION),
                TextureFilterConfig(MinFilter.NEAREST, MagFilter.NEAREST),
                GL14.GL_DEPTH_COMPONENT24, GL11.GL_REPEAT)
            )
        ),
        AREALIGHT_SHADOWMAP_RESOLUTION,
        AREALIGHT_SHADOWMAP_RESOLUTION,
        listOf(ColorAttachmentDefinition("Shadow", GL30.GL_RGBA32F, TextureFilterConfig(MinFilter.NEAREST_MIPMAP_LINEAR, MagFilter.LINEAR))).toCubeMaps(engine.gpuContext, AREALIGHT_SHADOWMAP_RESOLUTION, AREALIGHT_SHADOWMAP_RESOLUTION),
        "AreaLight Shadow"
    ))

    private val areaShadowPassProgram: Program = engine.run {
        programManager.getProgram(EngineAsset("shaders/mvp_entitybuffer_vertex.glsl"), EngineAsset("shaders/shadowmap_fragment.glsl"))
    }
    private val areaLightDepthMaps = ArrayList<Int>().apply {
        engine.gpuContext.invoke {
            for (i in 0 until MAX_AREALIGHT_SHADOWMAPS) {
                val renderedTextureTemp = engine.gpuContext.genTextures()
                engine.gpuContext.bindTexture(GlTextureTarget.TEXTURE_2D, renderedTextureTemp)
                GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA16, AREALIGHT_SHADOWMAP_RESOLUTION, AREALIGHT_SHADOWMAP_RESOLUTION, 0, GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, null as FloatBuffer?)
                GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR)
                GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR)
                GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE)
                GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE)
                add(renderedTextureTemp)
            }
        }
    }

    fun renderAreaLightShadowMaps(renderState: RenderState) {
        val areaLights = getComponents(AreaLight::class.java)

        profiled("Arealight shadowmaps") {
            engine.gpuContext.depthMask = true
            engine.gpuContext.enable(GlCap.DEPTH_TEST)
            engine.gpuContext.disable(GlCap.CULL_FACE)
            mapRenderTarget.use(engine.gpuContext as GpuContext<OpenGl>, true) // TODO: Remove cast

            for (i in 0 until Math.min(MAX_AREALIGHT_SHADOWMAPS, areaLights.size)) {

                mapRenderTarget.setTargetTexture(areaLightDepthMaps[i], 0)

                engine.gpuContext.clearDepthAndColorBuffer()

                val light = areaLights[i]

                areaShadowPassProgram.use()
                areaShadowPassProgram.bindShaderStorageBuffer(3, renderState.entitiesBuffer)
                areaShadowPassProgram.setUniformAsMatrix4("viewMatrix", light.camera.viewMatrixAsBuffer)
                areaShadowPassProgram.setUniformAsMatrix4("projectionMatrix", light.camera.projectionMatrixAsBuffer)

                for (e in renderState.renderBatchesStatic) {
                    val drawLines = false
                    draw(renderState.vertexIndexBufferStatic.vertexBuffer,
                            renderState.vertexIndexBufferStatic.indexBuffer,
                            e,
                            areaShadowPassProgram, false, drawLines)
                }
            }
        }
    }

    fun getDepthMapForAreaLight(light: AreaLight): Int {
        return getDepthMapForAreaLight(getAreaLights(), areaLightDepthMaps, light)
    }

    fun getCameraForAreaLight(light: AreaLight): Camera {
        return light.camera
    }
    fun getShadowMatrixForAreaLight(light: AreaLight): FloatBuffer {
        return light.camera.viewProjectionMatrixAsBuffer
    }


    fun getAreaLights() = getComponents(AreaLight::class.java)

    override fun CoroutineScope.update(scene: Scene, deltaSeconds: Float) {
//        TODO: Resize with instance count
        this@AreaLightSystem.gpuAreaLightArray = this@AreaLightSystem.gpuAreaLightArray.enlarge(this@AreaLightSystem.getRequiredAreaLightBufferSize() * AreaLight.getBytesPerInstance())
        this@AreaLightSystem.gpuAreaLightArray.buffer.rewind()

        for((index, areaLight) in this@AreaLightSystem.getComponents(AreaLight::class.java).withIndex()) {
            val target = this@AreaLightSystem.gpuAreaLightArray.getAtIndex(index)
            target.trafo.set(areaLight.entity.transform)
            target.color.set(areaLight.color)
            target.dummy0 = -1
            target.widthHeightRange.x = areaLight.width
            target.widthHeightRange.y = areaLight.height
            target.widthHeightRange.z = areaLight.range
            target.dummy1 = -1
        }
        this@AreaLightSystem.gpuAreaLightArray
    }
    private fun getRequiredAreaLightBufferSize() =
            getComponents(AreaLight::class.java).sumBy { it.entity.instanceCount }


    override fun render(result: DrawResult, state: RenderState) {
        renderAreaLightShadowMaps(state)
    }

    override fun extract(renderState: RenderState) {
//        TODO: Use this stuff instead of uniforms, take a look at DrawUtils.java
//        currentWriteState.entitiesState.jointsBuffer.sizeInBytes = getRequiredAreaLightBufferSize()
//        gpuAreaLightArray.shrink(currentWriteState.entitiesState.jointsBuffer.buffer.capacity())
//        gpuAreaLightArray.copyTo(currentWriteState.entitiesState.jointsBuffer.buffer)

        renderState.lightState.areaLights = getAreaLights()
        renderState.lightState.areaLightDepthMaps = areaLightDepthMaps
    }

    companion object {
        @JvmField val MAX_AREALIGHT_SHADOWMAPS = 2
        @JvmField val AREALIGHT_SHADOWMAP_RESOLUTION = 512

        fun getDepthMapForAreaLight(areaLights: List<AreaLight>, depthMaps: List<Int>, light: AreaLight): Int {
            val index = areaLights.indexOf(light)
            return if (index >= MAX_AREALIGHT_SHADOWMAPS) {
                -1
            } else depthMaps[index]
        }
    }
}
package de.hanno.hpengine.engine.graphics.renderer.pipelines

import de.hanno.hpengine.engine.backend.EngineContext
import de.hanno.hpengine.engine.backend.gpuContext
import de.hanno.hpengine.engine.backend.programManager
import de.hanno.hpengine.engine.camera.Camera
import de.hanno.hpengine.engine.component.ModelComponent
import de.hanno.hpengine.engine.graphics.light.area.AreaLightSystem
import de.hanno.hpengine.engine.graphics.profiled
import de.hanno.hpengine.engine.graphics.renderer.AtomicCounterBuffer
import de.hanno.hpengine.engine.graphics.renderer.IndirectDrawDescription
import de.hanno.hpengine.engine.graphics.renderer.constants.GlTextureTarget
import de.hanno.hpengine.engine.graphics.renderer.constants.MagFilter
import de.hanno.hpengine.engine.graphics.renderer.constants.MinFilter
import de.hanno.hpengine.engine.graphics.renderer.constants.TextureFilterConfig
import de.hanno.hpengine.engine.graphics.renderer.drawstrategy.FirstPassResult
import de.hanno.hpengine.engine.graphics.renderer.drawstrategy.extensions.VoxelConeTracingExtension
import de.hanno.hpengine.engine.graphics.renderer.drawstrategy.renderHighZMap
import de.hanno.hpengine.engine.graphics.renderer.pipelines.Pipeline.Companion.HIGHZ_FORMAT
import de.hanno.hpengine.engine.graphics.renderer.rendertarget.DepthBuffer
import de.hanno.hpengine.engine.graphics.renderer.rendertarget.FrameBuffer
import de.hanno.hpengine.engine.graphics.renderer.rendertarget.RenderTarget
import de.hanno.hpengine.engine.graphics.shader.AbstractProgram
import de.hanno.hpengine.engine.graphics.shader.Program
import de.hanno.hpengine.engine.graphics.shader.define.Define
import de.hanno.hpengine.engine.graphics.shader.define.Defines
import de.hanno.hpengine.engine.graphics.state.RenderState
import de.hanno.hpengine.engine.model.texture.Texture2D
import de.hanno.hpengine.engine.model.texture.Texture2D.TextureUploadInfo.Texture2DUploadInfo
import de.hanno.hpengine.engine.model.texture.TextureDimension
import de.hanno.hpengine.engine.scene.VertexIndexBuffer
import de.hanno.hpengine.engine.vertexbuffer.multiDrawElementsIndirectCount
import org.lwjgl.opengl.ARBClearTexture
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL15
import org.lwjgl.opengl.GL31
import org.lwjgl.opengl.GL42
import org.lwjgl.opengl.GL43

open class GPUFrustumCulledPipeline @JvmOverloads constructor(private val engine: EngineContext,
                                                              useFrustumCulling: Boolean = true,
                                                              useBackfaceCulling: Boolean = true,
                                                              useLineDrawing: Boolean = true) : IndirectPipeline(engine, useFrustumCulling, useBackfaceCulling, useLineDrawing) {

    protected open fun getDefines() = Defines(Define.getDefine("FRUSTUM_CULLING", true))

    private var occlusionCullingPhase1Vertex: Program = engine.run { programManager.getProgram(EngineAsset("shaders/occlusion_culling1_vertex.glsl")) }
    private var occlusionCullingPhase2Vertex: Program = engine.run { programManager.getProgram(EngineAsset("shaders/occlusion_culling2_vertex.glsl")) }

    val appendDrawCommandsProgram = engine.run { programManager.getProgram(EngineAsset("append_drawcommands_vertex.glsl")) }
    val appendDrawCommandsComputeProgram = engine.run { programManager.getComputeProgram(EngineAsset("shaders/append_drawcommands_compute.glsl")) }


    private val highZBuffer = RenderTarget(
        gpuContext = engine.gpuContext,
        frameBuffer = FrameBuffer(engine.gpuContext, DepthBuffer(engine.gpuContext, AreaLightSystem.AREALIGHT_SHADOWMAP_RESOLUTION, AreaLightSystem.AREALIGHT_SHADOWMAP_RESOLUTION)),
        width = engine.config.width / 2,
        height = engine.config.height / 2,
        textures = listOf(Texture2D.invoke(
            gpuContext = engine.gpuContext,
            info = Texture2DUploadInfo(TextureDimension(engine.config.width / 2, engine.config.height / 2)),
            textureFilterConfig = TextureFilterConfig(MinFilter.NEAREST_MIPMAP_LINEAR, MagFilter.LINEAR),
            internalFormat = HIGHZ_FORMAT)
        ), name = "GPUCulledPipeline")


    override fun beforeDrawStatic(renderState: RenderState, program: Program, renderCam: Camera) {
        super.beforeDrawStatic(renderState, program, renderCam)
    }

    override fun beforeDrawAnimated(renderState: RenderState, program: Program, renderCam: Camera) {
        super.beforeDrawAnimated(renderState, program, renderCam)
    }

    override fun draw(renderState: RenderState, programStatic: Program, programAnimated: Program, firstPassResult: FirstPassResult) {
        profiled("Actual draw entities") {

            val drawDescriptionStatic = IndirectDrawDescription(renderState, renderState.renderBatchesStatic, programStatic, commandOrganizationStatic, renderState.vertexIndexBufferStatic, this::beforeDrawStatic, engine.config.debug.isDrawLines, renderState.camera)
            val drawDescriptionAnimated = IndirectDrawDescription(renderState, renderState.renderBatchesAnimated, programAnimated, commandOrganizationAnimated, renderState.vertexIndexBufferAnimated, this::beforeDrawAnimated, engine.config.debug.isDrawLines, renderState.camera)

            beforeDraw(drawDescriptionStatic, drawDescriptionAnimated)

            drawDescriptionStatic.draw()
            drawDescriptionAnimated.draw()

            firstPassResult.verticesDrawn += verticesCount
            firstPassResult.entitiesDrawn += entitiesCount
        }
    }
    private fun beforeDraw(drawDescriptionStatic: IndirectDrawDescription, drawDescriptionAnimated: IndirectDrawDescription) {
        ARBClearTexture.glClearTexImage(highZBuffer.renderedTexture, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, VoxelConeTracingExtension.ZERO_BUFFER)
        val cullAndRender = { profilerString: String,
                              phase: Pipeline.CoarseCullingPhase ->
            profiled(profilerString) {
                cullAndRender(drawDescriptionStatic, { beforeDrawStatic(drawDescriptionStatic.renderState, drawDescriptionStatic.program, drawDescriptionStatic.drawCam) }, phase.staticPhase)
                debugPrintPhase1(drawDescriptionStatic, Pipeline.CullingPhase.STATIC_ONE)
                cullAndRender(drawDescriptionAnimated, { beforeDrawAnimated(drawDescriptionAnimated.renderState, drawDescriptionAnimated.program, drawDescriptionAnimated.drawCam) }, phase.animatedPhase)
                debugPrintPhase1(drawDescriptionAnimated, Pipeline.CullingPhase.ANIMATED_ONE)
                renderHighZMap()
            }
        }
        cullAndRender("Cull&Render Phase1", Pipeline.CoarseCullingPhase.ONE)
    }

    private val highZProgram = engine.run { programManager.getComputeProgram(EngineAsset("shaders/highZ_compute.glsl"), Defines(Define.getDefine("SOURCE_CHANNEL_R", true))) }

    private fun renderHighZMap() = renderHighZMap(engine.gpuContext, depthMap, engine.config.width, engine.config.height, highZBuffer.renderedTexture, highZProgram)

    open var depthMap = engine.deferredRenderingBuffer.visibilityMap

    private fun debugPrintPhase1(drawDescription: IndirectDrawDescription, phase: Pipeline.CullingPhase) {
        if (engine.config.debug.isPrintPipelineDebugOutput) {
            GL11.glFinish()
            println("########### $phase ")

            fun printPhase1(commandOrganization: CommandOrganization) {
                with(commandOrganization) {
//                    println("Visibilities")
//                    Util.printIntBuffer(visibilityBuffers.buffer.asIntBuffer(), if (commandCount == 0) 0 else commands.map { it.primCount }.reduce({ a, b -> a + b }), 1)
//                    println("Entity instance counts")
//                    Util.printIntBuffer(entitiesCounters.buffer.asIntBuffer(), commands.size, 1)
//                    println("DrawCountBuffer")
//                    println(drawCountBuffer.intBufferView.get(0))
//                    println("Entities compacted counter")
//                    println(entitiesCompactedCounter.intBufferView.get(0))
//                    println("Offsets culled")
//                    Util.printIntBuffer(entityOffsetBuffersCulled.buffer.asIntBuffer(), commands.size, 1)
//                    println("CurrentCompactedPointers")
//                    Util.printIntBuffer(currentCompactedPointers.buffer.asIntBuffer(), commands.size, 1)
//                    println("CommandOffsets")
//                    Util.printIntBuffer(commandOffsets.buffer.asIntBuffer(), commands.size, 1)
//                    if(commands.isNotEmpty()) {
//                        println("Commands")
//                        Util.printIntBuffer(commandBuffer.buffer.asIntBuffer(), 5, drawCountBuffers.intBufferView.get(0))
//                        println("Entities")
//                        Util.printModelComponents(drawDescription.renderState.entitiesBuffer.buffer, 10)
//                        println("Entities compacted")
//                        Util.printModelComponents(entitiesBuffersCompacted.buffer, 10)
//                    }
                }
            }

            printPhase1(drawDescription.commandOrganization)
        }
    }

    private fun cullAndRender(drawDescription: IndirectDrawDescription, beforeRender: () -> Unit, phase: Pipeline.CullingPhase) {
        with(drawDescription.commandOrganization) {
            val drawCountBuffer = drawCountBuffers
            drawCountBuffer.put(0, 0)
            val entitiesCompactedCounter1 = entitiesCompactedCounter
            entitiesCompactedCounter1.put(0,0)
            if (drawDescription.commandOrganization.commandCount == 0) {
                return
            }
            val targetCommandBuffer = commandBuffer

            with(drawDescription) {
                cullPhase(renderState, commandOrganization, drawCountBuffer, targetCommandBuffer, phase, cullCam)
                render(renderState, program, commandOrganization, vertexIndexBuffer, drawCountBuffer, targetCommandBuffer, entityOffsetBuffersCulled, beforeRender, phase, drawDescription.isDrawLines)
            }
        }
    }

    private fun cullPhase(renderState: RenderState,
                          commandOrganization: CommandOrganization,
                          drawCountBuffer: AtomicCounterBuffer,
                          targetCommandBuffer: PersistentMappedStructBuffer<DrawElementsIndirectCommand>,
                          phase: Pipeline.CullingPhase, cullCam: Camera) = profiled("Culling Phase") {
        cull(renderState, commandOrganization, phase, cullCam)

        drawCountBuffer.put(0, 0)
        val appendProgram: AbstractProgram = if(engine.config.debug.isUseComputeShaderDrawCommandAppend) appendDrawCommandsComputeProgram else appendDrawCommandsProgram

        profiled("Buffer compaction") {

            with(commandOrganization) {
                val instanceCount = primitiveCount
                visibilityBuffers.resize(instanceCount)
                commandOrganization.entitiesBuffersCompacted.sizeInBytes = instanceCount * ModelComponent.bytesPerInstance
                val entitiesCountersToUse = entitiesCounters
                entitiesCountersToUse.resize(commandCount)
                with(appendProgram) {
                    use()
                    bindShaderStorageBuffer(1, entitiesCounters)
                    bindShaderStorageBuffer(2, drawCountBuffer)
                    bindShaderStorageBuffer(3, renderState.entitiesState.entitiesBuffer)
                    bindShaderStorageBuffer(4, entityOffsetBuffer)
                    bindShaderStorageBuffer(5, commandBuffer)
                    bindShaderStorageBuffer(7, targetCommandBuffer)
                    bindShaderStorageBuffer(8, entityOffsetBuffersCulled)
                    bindShaderStorageBuffer(9, visibilityBuffers)
                    bindShaderStorageBuffer(10, entitiesBuffersCompacted)
                    bindShaderStorageBuffer(11, entitiesCompactedCounter)
                    bindShaderStorageBuffer(12, commandOffsets)
                    bindShaderStorageBuffer(13, currentCompactedPointers)
                    setUniform("maxDrawCommands", commandCount)
                    if(engine.config.debug.isUseComputeShaderDrawCommandAppend) {
                        appendDrawCommandsComputeProgram.dispatchCompute(commandCount, 1, 1)
                    } else {
                        val invocationsPerCommand : Int = 4096//commands.map { it.primCount }.max()!!//4096
                        GL31.glDrawArraysInstanced(GL11.GL_TRIANGLES, 0, (invocationsPerCommand + 2) / 3 * 3, commandCount)
                    }
                    GL42.glMemoryBarrier(GL43.GL_SHADER_STORAGE_BARRIER_BIT or GL42.GL_TEXTURE_FETCH_BARRIER_BIT or GL42.GL_SHADER_IMAGE_ACCESS_BARRIER_BIT or GL42.GL_COMMAND_BARRIER_BIT)
                    GL42.glMemoryBarrier(GL42.GL_ALL_BARRIER_BITS)
                }
            }
        }
    }

    private fun render(renderState: RenderState,
                       program: Program,
                       commandOrganization: CommandOrganization,
                       vertexIndexBuffer: VertexIndexBuffer,
                       drawCountBuffer: AtomicCounterBuffer,
                       commandBuffer: PersistentMappedStructBuffer<DrawElementsIndirectCommand>,
                       offsetBuffer: PersistentMappedStructBuffer<IntStruct>,
                       beforeRender: () -> Unit,
                       phase: Pipeline.CullingPhase,
                       drawLines: Boolean) = profiled("Actually render") {
        program.use()
        beforeRender()
        program.setUniform("entityIndex", 0)
        program.setUniform("entityBaseIndex", 0)
        program.setUniform("indirect", true)
        val drawCountBufferToUse = drawCountBuffer
        if(engine.config.debug.isUseGpuOcclusionCulling) {
            program.bindShaderStorageBuffer(3, commandOrganization.entitiesBuffersCompacted)
            program.bindShaderStorageBuffer(4, commandOrganization.entityOffsetBuffersCulled)
        } else {
            program.bindShaderStorageBuffer(3, renderState.entitiesState.entitiesBuffer)
            program.bindShaderStorageBuffer(4, offsetBuffer)
        }
//        Check out why this is necessary to be bound
        program.bindShaderStorageBuffer(3, commandOrganization.entitiesBuffersCompacted)
        program.bindShaderStorageBuffer(4, commandOrganization.entityOffsetBuffersCulled)
        program.bindShaderStorageBuffer(6, renderState.entitiesState.jointsBuffer)
        vertexIndexBuffer.multiDrawElementsIndirectCount(commandBuffer, drawCountBufferToUse, 0, commandOrganization.commandCount, drawLines)
    }

    private fun cull(renderState: RenderState,
                     commandOrganization: CommandOrganization,
                     phase: Pipeline.CullingPhase, cullCam: Camera) = profiled("Visibility detection") {
        val occlusionCullingPhase = if (phase.coarsePhase == Pipeline.CoarseCullingPhase.ONE) occlusionCullingPhase1Vertex else occlusionCullingPhase2Vertex
        with(occlusionCullingPhase) {
//            commandOrganization.commands.map { it.primCount }.reduce({ a, b -> a + b })
            val invocationsPerCommand : Int = 4096 // commandOrganization.commands.map { it.primCount }.max()!!
            use()
            with(commandOrganization) {
                bindShaderStorageBuffer(1, entitiesCounters)
                bindShaderStorageBuffer(2, drawCountBuffer)
                bindShaderStorageBuffer(3, renderState.entitiesState.entitiesBuffer)
                bindShaderStorageBuffer(4, entityOffsetBuffer)
                bindShaderStorageBuffer(5, commandBuffer)
                bindShaderStorageBuffer(8, entityOffsetBuffersCulled)
                bindShaderStorageBuffer(9, visibilityBuffers)
                bindShaderStorageBuffer(10, entitiesBuffersCompacted)
                bindShaderStorageBuffer(11, entitiesCompactedCounter)
                bindShaderStorageBuffer(12, commandOffsets)
                bindShaderStorageBuffer(13, currentCompactedPointers)
            }
            setUniform("maxDrawCommands", commandOrganization.commandCount)
            val camera = cullCam
            setUniformAsMatrix4("viewProjectionMatrix", camera.viewProjectionMatrixAsBuffer)
            setUniformAsMatrix4("viewMatrix", camera.viewMatrixAsBuffer)
            setUniform("camPosition", camera.entity.transform.position)
            setUniformAsMatrix4("projectionMatrix", camera.projectionMatrixAsBuffer)
            engine.gpuContext.bindTexture(0, GlTextureTarget.TEXTURE_2D, highZBuffer.renderedTexture)
            engine.gpuContext.bindImageTexture(1, highZBuffer.renderedTexture, 0, false, 0, GL15.GL_WRITE_ONLY, Pipeline.HIGHZ_FORMAT)
            GL31.glDrawArraysInstanced(GL11.GL_TRIANGLES, 0, (commandOrganization.commandCount + 2) / 3 * 3, invocationsPerCommand)
            GL42.glMemoryBarrier(GL43.GL_SHADER_STORAGE_BARRIER_BIT)
            GL42.glMemoryBarrier(GL42.GL_ALL_BARRIER_BITS)
        }
    }

}
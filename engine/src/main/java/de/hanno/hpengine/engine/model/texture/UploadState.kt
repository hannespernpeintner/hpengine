package de.hanno.hpengine.engine.model.texture

enum class UploadState {
    NOT_UPLOADED,
    UPLOADING,
    UPLOADED
}
package de.hanno.hpengine.engine.event

class MeshSelectedEvent(val entityIndex: Int, val meshIndex: Int)
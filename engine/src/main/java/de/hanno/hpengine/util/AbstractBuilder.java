package de.hanno.hpengine.util;

public abstract class AbstractBuilder<RETURN_TYPE> {
    public abstract RETURN_TYPE build();
}

package de.hanno.hpengine.util.ressources

interface Loadable {
    fun load() { }
    fun unload() { }
}